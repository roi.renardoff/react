import React from 'react'

class Film extends React.Component {
	state = {
		detailView: false,
	}
	renderDetailInfo = () => {
		const { films } = this.props
		return (
			<>
				<p>Episode Id: {films.episodeId}</p>
				<p>Opening crawl: {films.openingCrawl}</p>
			</>
		)
	}
	openDetailInfo = () => {
		this.setState({
			detailView: true,
		})
	}

	render() {
		const { detailView } = this.state
		const { films } = this.props
		return (
			<li>
				<p>{films.name}</p>
				{!detailView ? (
					<button onClick={this.openDetailInfo}>More</button>
				) : (
					this.renderDetailInfo()
				)}
			</li>
		)
	}
}

export default Film
