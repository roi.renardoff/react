import './App.scss';
import { Film, Loader } from './components';
// import Film from './components/Films';
import axios from 'axios'
import React from 'react';

class App extends React.Component {
	state = {
		films: [],
    loading: true
	}

	async componentDidMount() {
    const {data} = await  axios.get('https://ajax.test-danit.com/api/swapi/films')
    this.setState({
      films: data,
      loading: false
    })
    console.log(data);

  }
	render() {
    const {films, loading} = this.state
    return (
			<div className='App'>
				{loading ? (
					<Loader />
				) : (
					<ol>
						{films.map(el => (
							<Film key={el.id} films={el} />
						))}
					</ol>
				)}
			</div>
		)
	}
}

export default App;
